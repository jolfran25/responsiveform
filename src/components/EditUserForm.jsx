import React, { useState, useEffect } from 'react'

const EditUserForm = props => {


    useEffect(() => {
        setUser(props.currentUser)
    }, [props])

    const [user, setUser] = useState(props.currentUser)

    const handleInputChange = event => {
        const { modelo, value } = event.target

        setUser({ ...user, [modelo]: value })
    }

    return (
        <form
        onSubmit={event => {
            event.preventDefault()

            props.updateUser(user.id, user)
        }}
        >
        <label>Modelo</label>
        <input type="text" name="modelo" value={user.modelo} onChange={handleInputChange} />
        <label>Marca</label>
        <input type="text" name="marca" value={user.marca} onChange={handleInputChange} />
        <label>Año</label>
        <input type="text" name="año" value={user.año} onChange={handleInputChange} />
        <label>Dueño</label>
        <input type="text" name="dueño" value={user.dueño} onChange={handleInputChange} />
        <label>Precio</label>
        <input type="text" name="precio" value={user.precio} onChange={handleInputChange} />
        <button>Update user</button>
        <button onClick={() => props.setEditing(false)} className="button muted-button">
            Cancel
        </button>
        </form>
    )
}

export default EditUserForm