import React from 'react'
import { useForm } from 'react-hook-form'

const EditYo = (props) => {

    const {register, errors, handleSubmit, setValue} = useForm({
        defaultValues: props.currentUser
    });

    setValue('modelo', props.currentUser.modelo)
    setValue('marca', props.currentUser.marca)
    setValue('año', props.currentUser.año)
    setValue('dueño', props.currentUser.dueño)
    setValue('precio', props.currentUser.precio)

    const onSubmit = (data, e) => {
        data.id = props.currentUser.id
        console.log(data)
        props.updateUser(props.currentUser.id, data)
        e.target.reset()
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <label>Modelo</label>
            <input 
                type="text" 
                name="modelo"
                ref={register({required: {value: true, message: 'Valor requerido'}})}
                />
            <div>
                {errors?.modelo?.message}
            </div>
            <label>marca</label>
            <input 
                type="text" 
                name="marca" 
                ref={register({required: {value: true, message: 'Valor requerido'}})}
                />
            <div>
                {errors?.marca?.message}
            </div>
            <label>año</label>
            <input 
                type="text" 
                name="año" 
                ref={register({required: {value: true, message: 'Valor requerido'}})}
                />
            <div>
                {errors?.año?.message}
            </div>
            <label>dueño</label>
            <input 
                type="text" 
                name="dueño" 
                ref={register({required: {value: true, message: 'Valor requerido'}})}
                />
            <div>
                {errors?.dueño?.message}
            </div>
            <label>precio</label>
            <input 
                type="text" 
                name="precio" 
                ref={register({required: {value: true, message: 'Valor requerido'}})}
                />
            <div>
                {errors?.precio?.message}
            </div>
            <button type="submit">Edit user</button>
            <button onClick={() => props.setEditing(false)} className="button muted-button">
                Cancel
            </button>
        </form>
    );
}
 
export default EditYo;