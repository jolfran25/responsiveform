import React from 'react';
import { useForm } from 'react-hook-form'

const AddUserForm = (props) => {

    const {register, errors, handleSubmit} = useForm();

    const onSubmit = (data, e) => {
        data.id = null
        console.log(data)
        props.addUser(data)
        e.target.reset();
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <h4>tipo de auto</h4>
            <div className={'alternate-background contain-table vertical '}>
            <label className={'inline-block '}>
            Camion
            <input 
                type="radio" 
                name="auto1"                
                />
            </label>
            <label>
                Automovil
                <input 
                type="radio" 
                name="auto2"               
                />
            </label>
            <div>
                {errors?.modelo?.message}
            </div>
            </div>
            <label>Modelo</label>
            <input 
                type="text" 
                name="modelo"
                ref={register({required: {value: true, message: 'Valor requerido'}})}
                />
            <div>
                {errors?.modelo?.message}
            </div>
            <label>Marca</label>
            <input 
                type="text" 
                name="marca" 
                ref={register({required: {value: true, message: 'Valor requerido'}})}
                />
            <div>
                {errors?.marca?.message}
            </div>
            <label>Año</label>
            <input 
                type="text" 
                name="año" 
                ref={register({required: {value: true, message: 'Valor requerido'}})}
                />
            <div>
                {errors?.año?.message}
            </div>
            <label>Dueño</label>
            <input 
                type="text" 
                name="dueño" 
                ref={register({required: {value: true, message: 'Valor requerido'}})}
                />
            <div>
                {errors?.dueño?.message}
            </div>
            <label>Precio</label>
            <input 
                type="text" 
                name="precio" 
                ref={register({required: {value: true, message: 'Valor requerido'}})}
                />
            <div>
                {errors?.precio?.message}
            </div>
            <button type="submit">Add new user</button>
        </form>
    );
}
 
export default AddUserForm;