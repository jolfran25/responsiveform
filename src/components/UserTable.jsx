import React from 'react'

const UserTable = (props) => (
  <table>
    <thead>
      <tr>
        <th>Icono</th>
        <th>Modelo</th>
        <th>Marca</th>
        <th>Año</th>
        <th>Dueño</th>
        <th>Precio</th>
        <th>Acciones</th>        
      </tr>
    </thead>
    <tbody>
        {
            props.users.length > 0 ? (
                props.users.map(user => (
                    <tr key={user.id}>
                        <td><image src={user.auto}></image></td>
                        <td>{user.modelo}</td>
                        <td>{user.marca}</td>
                        <td>{user.año}</td>
                        <td>{user.dueño}</td>
                        <td>{user.precio}</td>                        
                        <td>
                        <button 
                            className="button muted-button"
                            onClick={() => {
                                props.editRow(user)
                            }}
                            >
                            Edit
                        </button>
                        <button 
                            className="button muted-button"
                            onClick={() => props.deleteUser(user.id)}
                            >
                            Delete
                        </button>
                        </td>
                    </tr>
                ))
            ) : (
                <tr>
                    <td colSpan={3}>No users</td>
                </tr>
            )
        }

      
    </tbody>
  </table>
)

export default UserTable